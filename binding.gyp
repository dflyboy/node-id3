{
  "targets": [
    {
      "target_name": "node-id3",
      "sources": [ "addon.cpp", "musicparser.cpp"],
      "cflags": ["-Wall", "-std=c++11"],
      "cflags_cc": ["-fexceptions"],
      "cflags_cc!": [ "-fno-rtti" ],
      "include_dirs" : ["<!(node -e \"require('nan')\")"],
      "link_settings": {
        "libraries": ["-ltag", "-lcryptopp"]
      },
      "conditions": [
        [ 'OS=="mac"', {
            "xcode_settings": {
                'OTHER_CPLUSPLUSFLAGS' : ['-std=c++11','-stdlib=libc++'],
                'OTHER_LDFLAGS': ['-stdlib=libc++'],
                'MACOSX_DEPLOYMENT_TARGET': '10.13',
                'GCC_ENABLE_CPP_RTTI': 'YES',
                'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
              }
            }
        ],
        [
          'OS=="win"', {
            "include_dirs": ["D:/projects/libraries/cryptopp.5.6.5.4/build/native/include", "D:/tools/taglibcpp.1.11.1/lib/native/include"]
          }
        ]
      ]
    }
  ]
}
