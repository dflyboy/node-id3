#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <cstdio>
#include <ctime>

#include <string>
#include "musicparser.h"

using namespace boost::filesystem;
using namespace std;

int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        path p(argv[1]);

        if (is_directory(p))
        {
            vector<string> files, tracks;

            for (auto &entry : boost::make_iterator_range(directory_iterator(p), {}))
                files.push_back(entry.path().string());

            cout << "found " << files.size() << " files" << endl;

            BOOST_FOREACH (string file, files)
            {
                string format = file.substr(file.length() - 3);
                if (format == "m4a" || format == "mp3")
                {
                    tracks.push_back(file);
                }
            }

            cout << "found " << tracks.size() << " tracks" << endl;

            vector<track_t> result;
            clock_t begin = clock();

            BOOST_FOREACH (string file, tracks)
            {
                track_t track = parseTrack(file.c_str());
                result.push_back(track);

                cout << track.title << endl
                     << track.album << endl
                     << track.year << endl
                     << track.duration << endl
                     << track.coverFormat << " cover is " << track.cover.size() << " byte large" << endl
                     << "hash: " << track.coverHash << endl
                     << endl;
            }

            clock_t end = clock();
            double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

            std::ofstream ofs;
            ofs.open("cover." + result[0].coverFormat.substr(6), std::ofstream::out | std::ofstream::binary);

            ofs.write(result[0].cover.data(), result[0].cover.size());

            ofs.close();

            cout << "Done parsing in " << elapsed_secs * 1000 << "ms" << endl;
        }

        // track_t track = parseTrack(filename);
        // cout << track.title << endl
        //      << track.album << endl
        //      << track.year << endl
        //      << track.duration << endl;
    }
    return 0;
}