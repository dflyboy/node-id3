#include <string>
#include <taglib/fileref.h>
#include <taglib/mpegfile.h>
#include <taglib/tag.h>
#include <taglib/id3v2tag.h>
#include <taglib/id3v2frame.h>
#include <taglib/mp4file.h>
#include <taglib/attachedpictureframe.h>

typedef unsigned int small_num;

struct track_t
{
    std::string file;

    std::string title;
    std::string album;
    std::string artist;
    small_num year;
    small_num duration;
    small_num trackNo;

    std::string coverFormat;
    char* coverData;
    size_t coverSize;
    std::string coverHash;
};

int getMp4Image(TagLib::MP4::Tag *tag, char **cover, size_t *size, std::string *format);
int getMp3Image(TagLib::ID3v2::Tag *m_tag, char **cover, size_t *size, std::string *format);
track_t parseTrack(const char *file);