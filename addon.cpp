#include <nan.h>
#include <cstdio>
#include "musicparser.h"

using namespace std;
using namespace Nan;
using namespace v8;

class ParseWorker : public AsyncWorker
{
  public:
    ParseWorker(Callback *callback, std::string file)
        : AsyncWorker(callback), file(file) {}

    ~ParseWorker() {}

    void Execute()
    {
        // cout << file << endl;
        track = parseTrack(file.c_str());
    }

    // We have the results, and we're back in the event loop.
    void HandleOKCallback()
    {
        // Nan::HandleScope scope;
        Local<String> pArtist = Nan::New<String>("artist").ToLocalChecked();
        Local<String> pAlbum = Nan::New<String>("album").ToLocalChecked();
        Local<String> pTitle = Nan::New<String>("title").ToLocalChecked();
        Local<String> pFile = Nan::New<String>("file").ToLocalChecked();

        Local<String> pYear = Nan::New<String>("year").ToLocalChecked();
        Local<String> pDuration = Nan::New<String>("duration").ToLocalChecked();
        Local<String> pTrack = Nan::New<String>("trackNo").ToLocalChecked();

        Local<String> pCFormat = Nan::New<String>("coverFormat").ToLocalChecked();
        Local<String> pCHash = Nan::New<String>("coverHash").ToLocalChecked();
        Local<String> pCData = Nan::New<String>("coverData").ToLocalChecked();

        // create the return object
        Local<Object> retval = Nan::New<Object>();

        if (track.coverSize > 0 && track.coverData != NULL)
        {
            v8::Local<v8::Object> cover = Nan::NewBuffer(track.coverData, track.coverSize).ToLocalChecked();
            Nan::Set(retval, pCData, cover);
        }

        // set the properties on the return object
        Nan::Set(retval, pArtist, Nan::New<v8::String>(track.artist.c_str()).ToLocalChecked());
        Nan::Set(retval, pAlbum, Nan::New<v8::String>(track.album.c_str()).ToLocalChecked());
        Nan::Set(retval, pTitle, Nan::New<v8::String>(track.title.c_str()).ToLocalChecked());
        // Nan::Set(retval, pFile, Nan::New<v8::String>(track.file.c_str()).ToLocalChecked());

        Nan::Set(retval, pYear, Nan::New<Number>(track.year));
        Nan::Set(retval, pDuration, Nan::New<Number>(track.duration));
        Nan::Set(retval, pTrack, Nan::New<Number>(track.trackNo));

        Nan::Set(retval, pCFormat, Nan::New<v8::String>(track.coverFormat.c_str()).ToLocalChecked());
        Nan::Set(retval, pCHash, Nan::New<v8::String>(track.coverHash.c_str()).ToLocalChecked());

        Local<Value> argv[] = {
            Null(),
            retval};

        callback->Call(2, argv);
    }

  private:
    std::string file;
    track_t track;
    char *coverData;
};

NAN_METHOD(ParseTrack)
{
    // Nan::HandleScope scope;
    // Make property names to access the input object
    Local<String> pArtist = Nan::New<String>("artist").ToLocalChecked();
    Local<String> pAlbum = Nan::New<String>("album").ToLocalChecked();
    Local<String> pTitle = Nan::New<String>("title").ToLocalChecked();

    Local<String> pYear = Nan::New<String>("year").ToLocalChecked();
    Local<String> pDuration = Nan::New<String>("duration").ToLocalChecked();
    Local<String> pTrack = Nan::New<String>("trackNo").ToLocalChecked();

    Local<String> pCFormat = Nan::New<String>("coverFormat").ToLocalChecked();
    Local<String> pCHash = Nan::New<String>("coverHash").ToLocalChecked();
    Local<String> pCData = Nan::New<String>("coverData").ToLocalChecked();

    // create the return object
    Local<Object> retval = Nan::New<Object>();

    v8::Local<v8::String> v8_string(info[0]->ToString());
    Nan::Utf8String nan_string(v8_string);

    std::string file(*nan_string);
    track_t track = parseTrack(file.c_str());
    // char *coverData = (char *)malloc(track.cover.size());
    // memcpy(coverData, track.cover.data(), track.cover.size());
    if (track.coverSize > 0 && track.coverData != NULL)
    {
        v8::Local<v8::Object> cover = Nan::NewBuffer(track.coverData, track.coverSize).ToLocalChecked();
        Nan::Set(retval, pCData, cover);
    }
    // set the properties on the return object
    Nan::Set(retval, pArtist, Nan::New<v8::String>(track.artist.c_str()).ToLocalChecked());
    Nan::Set(retval, pAlbum, Nan::New<v8::String>(track.album.c_str()).ToLocalChecked());
    Nan::Set(retval, pTitle, Nan::New<v8::String>(track.title.c_str()).ToLocalChecked());

    Nan::Set(retval, pYear, Nan::New<Number>(track.year));
    Nan::Set(retval, pDuration, Nan::New<Number>(track.duration));
    Nan::Set(retval, pTrack, Nan::New<Number>(track.trackNo));

    Nan::Set(retval, pCFormat, Nan::New<v8::String>(track.coverFormat.c_str()).ToLocalChecked());
    Nan::Set(retval, pCHash, Nan::New<v8::String>(track.coverHash.c_str()).ToLocalChecked());

    info.GetReturnValue().Set(retval);
}

NAN_METHOD(ParseAsync)
{
    // Nan::HandleScope scope;
    if (info.Length() < 1)
    {
        return;
    }
    if (!info[0]->IsString())
    {
        return;
    }

    // wrap the string with v8's string type
    v8::Local<v8::String> v8_string(info[0]->ToString());
    Nan::Utf8String nan_string(v8_string);
    std::string file(*nan_string);

    Callback *callback = new Callback(info[1].As<Function>());

    AsyncQueueWorker(new ParseWorker(callback, file.c_str()));
}

NAN_MODULE_INIT(Init)
{
    Nan::Set(target, New<String>("parseTrack").ToLocalChecked(),
             GetFunction(New<FunctionTemplate>(ParseTrack)).ToLocalChecked());
    Nan::Set(target, New<String>("parseAsync").ToLocalChecked(),
             GetFunction(New<FunctionTemplate>(ParseAsync)).ToLocalChecked());
}

// macro to load the module when require'd
NODE_MODULE(my_addon, Init)