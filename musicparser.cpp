#include "musicparser.h"
#include <cryptopp/adler32.h>
#include <cryptopp/base64.h>
#include <cryptopp/sha.h>

using namespace std;

string coverHash(const char *data, size_t len)
{
    CryptoPP::Adler32 algo;
    // CryptoPP::SHA1 algo;

    algo.Update((unsigned char *)data, len);
    unsigned int size = algo.DigestSize();
    unsigned char hash[size];
    algo.TruncatedFinal(hash, size);

    string encoded;

    CryptoPP::Base64Encoder encoder(nullptr, false);
    encoder.Put(hash, size);
    encoder.MessageEnd();

    CryptoPP::word64 osize = encoder.MaxRetrievable();

    encoded.resize(osize);
    encoder.Get((unsigned char *)&encoded[0], encoded.size());

    return encoded;
}

int getMp4Image(TagLib::MP4::Tag *tag, char **cover, size_t *size, string *format)
{
    TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
    TagLib::MP4::Item coverItem = itemsListMap["covr"];
    TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
    if (coverArtList.isEmpty())
    {
        *size = 0;
        return 0;
    }

    TagLib::MP4::CoverArt coverArt = coverArtList.front();

    *size = coverArt.data().size();
    *cover = (char*) malloc(*size);
    memcpy(*cover, coverArt.data().data(), *size);

    using namespace TagLib::MP4;

    switch (coverArt.format())
    {
    case TagLib::MP4::CoverArt::BMP:
        *format = "image/bmp";
        break;
    case TagLib::MP4::CoverArt::GIF:
        *format = "image/gif";
        break;
    case TagLib::MP4::CoverArt::JPEG:
        *format = "image/jpeg";
        break;
    case TagLib::MP4::CoverArt::PNG:
        *format = "image/png";
        break;
    case TagLib::MP4::CoverArt::Unknown:
        break;
    }

    return *size;
}

int getMp3Image(TagLib::ID3v2::Tag *m_tag, char **cover, size_t *size, string *format)
{
    TagLib::ID3v2::FrameList frameList = m_tag->frameList("APIC");
    if (frameList.isEmpty())
    {
        *size = 0;
        return 0;
    }

    TagLib::ID3v2::AttachedPictureFrame *coverImg = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(frameList.front());
    *size = coverImg->picture().size();
    *cover = (char*) malloc(*size);
    memcpy(*cover, coverImg->picture().data(), *size);

    *format = coverImg->mimeType().toCString();

    return *size;
}

track_t parseTrack(const char *file)
{
    track_t item;
    item.file = string(file);

    TagLib::FileRef f(file);
    // TagLib::FileRef()
    TagLib::Tag *tag = f.tag();

    if (tag)
    {

        item.title = tag->title().to8Bit();
        item.album = tag->album().to8Bit();
        item.artist = tag->artist().to8Bit();

        item.trackNo = tag->track();
        item.year = tag->year();

        item.duration = f.audioProperties()->lengthInSeconds();

        string format = item.file.substr(item.file.length() - 3);
        string iFormat;
        int result;

        if (format == "mp3")
        {
            TagLib::MPEG::File *mpFile = static_cast<TagLib::MPEG::File *>(f.file());
            result = getMp3Image(mpFile->ID3v2Tag(), &item.coverData, &item.coverSize, &iFormat);
            if(result > 0) item.coverHash = coverHash(item.coverData, item.coverSize);
        }
        else if (format == "m4a")
        {
            result = getMp4Image(static_cast<TagLib::MP4::Tag *>(tag), &item.coverData, &item.coverSize, &iFormat);
            if(result > 0) item.coverHash = coverHash(item.coverData, item.coverSize);
        }
        item.coverFormat = iFormat;
    }

    return item;
}