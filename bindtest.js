const util = require('util');
const fs = require('fs');

var binding = require('./build/Release/node-id3');

var result = binding.parseTrack(process.argv[2]);

console.log('Sync parse', util.inspect(result));

binding.parseAsync(process.argv[2], (e, res) => {
    console.log('Async parse', util.inspect(res));
    fs.writeFile('cover.jpg', res.coverData, () => console.log('write done'));
});